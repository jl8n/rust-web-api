use actix_web::{App, web, get, post, HttpResponse, HttpServer, Responder, Result};
use serde::{Deserialize, Serialize};
//use std::env;
use std::fs;


#[derive(Serialize, Deserialize)]
struct CovidCases {
    name: String,
    data: String,
}


fn read_cases_file() {
    let filename = "sw-utah-covid-cases.csv";
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    
        println!("contents:\n{}", contents);
}


#[get("/")]
async fn get_root() -> impl Responder {
    println!("GET /");
    HttpResponse::Ok().body("Hey there Jimbo")
}

#[get(r"/api/{name}")]
async fn get_api(obj: web::Path<CovidCases>) -> Result<HttpResponse> {
    println!("GET /api");
    Ok(HttpResponse::Ok().json(CovidCases {
        name: obj.name.to_string(),
        data: obj.data.to_string(),
    }))
}


#[post("/")]
async fn post_root() -> Result<String> {
    println!("POST /");
    Ok(format!("Welcome!"))
}


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    read_cases_file();
    HttpServer::new(|| {
        App::new()
            .service(get_root)
            .service(post_root)
            .service(get_api)
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
